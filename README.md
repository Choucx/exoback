# Creation d'une application backend

## Instructions
Le but est de faire un backend en C# avec ASP.NET et MVC.NET et le livré sur un Git distant Publique.

1. Créer un nouveau depo git public
2. Créer un nouveau projet .NET
    * Il consiste en deux CRUD de voiture et personne :
    * Une voiture possède:
        * un nom
        * une marque
        * une imatriculation
        * un proprietaire
    * Un utilisateur possède:
        * un nom
        * un prenom
        * un age
    * Il y aura deux Controllers (VoitureController et PersonneController)
    * Il y aura deux services.
    * Il y aura deux repository
    * Ils sont reliés par IOD (injection de dependance)
    * Le controleur peut etre un bouchon (utilisation d'une liste)
3. En Bonus:
    * Retourner la liste des voitures d'un utilisateur
    * Connecter a une Base de donnée
    * Changer le Repository pour qu'il lance les requetes grace a Entity
    * N'ajouter la voiture uniquement si le propriétaire est majeur.
    * Faire le retour des status et la gestion des erreurs.
    * Documenter votre code
 

