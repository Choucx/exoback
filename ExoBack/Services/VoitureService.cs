﻿using ExoBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Services
{
    public interface VoitureService
    {
        public Voiture Sauvegarde(Voiture voiture);
        public IEnumerable<Voiture> TrouverTous();
        public IEnumerable<Voiture> TrouverToutesParUtilisateurId(int id);
        public Voiture TrouverParId(int id);
        public Voiture Modifier(Voiture voiture);
        public void Supprimer(int id);
        public int Compte();
    }
}
