﻿using ExoBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Services
{
    public interface UtilisateurService
    {
        public Utilisateur Sauvegarde(Utilisateur utilisateur);
        public IEnumerable<Utilisateur> TrouverTous();
        public Utilisateur TrouverParId(int id);
        public Utilisateur Modifier(Utilisateur utilisateur);
        public void Supprimer(int id);
        public int Compte();
    }
}
