﻿using ExoBack.Models;
using ExoBack.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Services.Impl
{
    public class VoitureImplService: VoitureService
    {
        private VoitureRepository repository;
        private UtilisateurService uService;

        public VoitureImplService(VoitureRepository repository, UtilisateurService uService)
        {
            this.repository = repository;
            this.uService = uService;
        }

        public int Compte()
        {
            return this.repository.Count();
        }

        public Voiture Sauvegarde(Voiture voiture)
        {
            Voiture resultat = null;

            if (voiture.Proprietaire != null && this.uService.TrouverParId(voiture.Proprietaire.Id) != null)
            {
                voiture.Proprietaire = this.uService.TrouverParId(voiture.Proprietaire.Id);
                resultat = this.repository.Save(voiture);
            }

            return resultat;
        }

        public IEnumerable<Voiture> TrouverTous()
        {
            return this.repository.FindAll();
        }

        public Voiture TrouverParId(int id)
        {
            Voiture resultat = null;

            if (this.repository.Count() >= id)
            {
                resultat = this.repository.FindById(id);
            }
            return resultat;
        }

        public Voiture Modifier(Voiture voiture)
        {
            Voiture resultat = null;
            if (voiture.Id <= this.repository.Count() && this.uService.TrouverParId(voiture.Proprietaire.Id) != null)
            {
                voiture.Proprietaire = this.uService.TrouverParId(voiture.Proprietaire.Id);
                resultat = voiture;
                this.repository.Update(voiture);

            }
            return resultat;
        }

        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }

        public IEnumerable<Voiture> TrouverToutesParUtilisateurId(int id)
        {

            List<Voiture> resultat = new List<Voiture>();

            if (this.uService.TrouverParId(id) != null)
            {
                resultat = this.repository.FindAll().Where(voiture => voiture.Proprietaire.Id == id).ToList();
            }

            return resultat;
        }
    }
}
