﻿using ExoBack.Models;
using ExoBack.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Services.Impl
{
    public class UtilisateurImplService:UtilisateurService
    {
        private UtilisateurRepository repository;

        public UtilisateurImplService(UtilisateurRepository repository)
        {
            this.repository = repository;
        }

        public int Compte()
        {
            return this.repository.Count();
        }

        public Utilisateur Sauvegarde(Utilisateur utilisateur)
        {
            return this.repository.Save(utilisateur);
        }

        public IEnumerable<Utilisateur> TrouverTous()
        {
            return this.repository.FindAll();
        }

        public Utilisateur TrouverParId(int id)
        {
            Utilisateur resultat = null;
            if (this.repository.Count() >= id)
            {
                resultat = this.repository.FindById(id);
            }
            return resultat;
        }

        public Utilisateur Modifier(Utilisateur utilisateur)
        {
            return this.repository.Update(utilisateur);
        }

        public void Supprimer(int id)
        {
            this.repository.Delete(id);
        }
    }
}
