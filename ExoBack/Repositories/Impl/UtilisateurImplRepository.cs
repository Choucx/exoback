﻿using ExoBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Repositories.Impl
{
    public class UtilisateurImplRepository: UtilisateurRepository
    {
        private List<Utilisateur> utilisateurs = new List<Utilisateur>();

        public int Count()
        {
            return this.utilisateurs.Count;
        }

        public void Delete(int id)
        {
            this.utilisateurs[id] = null;
        }

        public void Delete(Utilisateur utilisateur)
        {
            this.utilisateurs[utilisateur.Id] = null;
        }

        public IEnumerable<Utilisateur> FindAll()
        {
            return this.utilisateurs.Where(utilisateur => utilisateur != null);
        }

        public Utilisateur FindById(int id)
        {
            return this.utilisateurs[id];

        }

        public Utilisateur Save(Utilisateur utilisateur)
        {
            utilisateur.Id = this.utilisateurs.Count;
            this.utilisateurs.Add(utilisateur);
            return utilisateur;
        }

        public Utilisateur Update(Utilisateur utilisateur)
        {
            this.utilisateurs[utilisateur.Id] = utilisateur;
            return utilisateur;
        }
    }
}
