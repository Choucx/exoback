﻿using ExoBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Repositories.Impl
{
    public class VoitureImplRepository: VoitureRepository
    {
        private List<Voiture> voitures = new List<Voiture>();

        public int Count()
        {
            return this.voitures.Count();
        }

        public void Delete(int id)
        {
            this.voitures[id] = null;
        }

        public void Delete(Voiture voiture)
        {
            this.voitures[voiture.Id] = null;
        }

        public IEnumerable<Voiture> FindAll()
        {
            return this.voitures.Where(voiture => voiture != null);
        }

        public IEnumerable<Voiture> FindAllByUserID(int id)
        {
            return this.voitures.Where(voiture => voiture.Proprietaire.Id == id);
        }

        public Voiture FindById(int id)
        {
            return this.voitures[id];
        }

        public Voiture Save(Voiture voiture)
        {
            voiture.Id = this.voitures.Count;
            this.voitures.Add(voiture);
            return voiture;
        }

        public Voiture Update(Voiture voiture)
        {
            this.voitures[voiture.Id] = voiture;
            return voiture;
        }
    }
}
