﻿using ExoBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Repositories
{
    public interface UtilisateurRepository
    {
        public Utilisateur Save(Utilisateur utilisateur);
        public IEnumerable<Utilisateur> FindAll();
        public Utilisateur FindById(int id);
        public Utilisateur Update(Utilisateur utilisateur);
        public void Delete(int id);
        public void Delete(Utilisateur utilisateur);
        public int Count();
    }
}
