﻿using ExoBack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExoBack.Repositories
{
    public interface VoitureRepository
    {
        public Voiture Save(Voiture voiture);
        public IEnumerable<Voiture> FindAll();
        public Voiture FindById(int id);
        public Voiture Update(Voiture voiture);
        public void Delete(int id);
        public void Delete(Voiture voiture);
        public int Count();
        public IEnumerable<Voiture> FindAllByUserID(int id);
    }
}
