﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExoBack.Models;
using ExoBack.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExoBack.Controllers
{
    [Route("voitures")]
    [ApiController]
    public class VoitureController : ControllerBase
    {
        private VoitureService vService;

        public VoitureController(VoitureService vService)
        {
            this.vService = vService;
        }

        [HttpPost]
        [Route("")]
        public Voiture Save([FromBody] Voiture voiture)
        {
            return this.vService.Sauvegarde(voiture);
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Voiture> FindAll()
        {
            return this.vService.TrouverTous();
        }

        [HttpGet]
        [Route("{id}")]
        public Voiture FindById(int id)
        {
            return this.vService.TrouverParId(id);
        }

        [HttpGet]
        [Route("utilisateur/{id}")]
        public IEnumerable<Voiture> FindAllByUserId(int id)
        {
            return this.vService.TrouverToutesParUtilisateurId(id);
        }

        [HttpPut]
        [Route("{id}")]
        public Voiture Update([FromBody] Voiture voiture)
        {
            return this.vService.Modifier(voiture);
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            this.vService.Supprimer(id);
        }
    }
}