﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExoBack.Models;
using ExoBack.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExoBack.Controllers
{
    [Route("utilisateurs")]
    [ApiController]
    public class UtilisateurController : ControllerBase
    {
        private UtilisateurService service;

        public UtilisateurController(UtilisateurService service)
        {
            this.service = service;
        }

        [HttpPost]
        [Route("")]
        public Utilisateur Save([FromBody] Utilisateur utilisateur)
        {
            return this.service.Sauvegarde(utilisateur);
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Utilisateur> FindAll()
        {
            return this.service.TrouverTous();
        }

        [HttpGet]
        [Route("{id}")]
        public Utilisateur FindById(int id)
        {
            return this.service.TrouverParId(id);
        }

        [HttpPut]
        [Route("")]
        public Utilisateur Update([FromBody] Utilisateur utilisateur)
        {
            return this.service.Modifier(utilisateur);
        }

        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            this.service.Supprimer(id);
        }
    }
}